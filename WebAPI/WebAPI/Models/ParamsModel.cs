﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class ParamsModel
    {
        public string GetType { get; set; }
        public int PymID { get; set; }
        public string Search { get; set; }

    }
}
