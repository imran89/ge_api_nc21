﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models.InfoDevice
{
    public class InfoDeviceModel
    {
        public string DI_Employee_NIK { get; set; }
        public string DI_CompanyID { get; set; }
        public string DI_LogHistory { get; set; }

    }
}
