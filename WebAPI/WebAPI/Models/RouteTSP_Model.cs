﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    
    public class RouteTSP_Model
    {
        public int Route_ID { get; set; }
        public string Route_EmployeeNik { get; set; }
        public string Route_Queque { get; set; }
        public int Route_Day { get; set; }
        public int Route_Freq { get; set; }
        public DateTime Route_SuggestionTime { get; set; }
        public string Route_Repeat { get; set; }
        public int Route_AreaID { get; set; }
        public int Route_CompanyID { get; set; }
        public DateTime Route_InsertOn { get; set; }
        public string Route_InsertBy { get; set; }
        public DateTime Route_UpdateOn { get; set; }
        public string Route_UpdateBy { get; set; }
        public Boolean Route_Type { get; set; }
        public Boolean Route_IsActive { get; set; }
        public Boolean Route_Isdelete { get; set; }
        public string Outlet_Code { get; set; }
        public string Outlet_Name { get; set; }
        public string Outlet_Latitude { get; set; }
        public string Outlet_Longitude { get; set; }
        public int Latitude { get; set; }
        public int Longitude { get; set; }
    }
    public class RouteOptimizeModel
    {
        public long Latitude { set; get; }
        public long Longitude { set; get; }
        public string Outlet_Code { set; get; }
        public string Outlet_Name { set; get; }
        public string Outlet_Latitude { set; get; }
        public string Outlet_Longitude { set; get; }

    }

    public class RouteValue
    {
        public int Index { set; get; }
        public long Distance { set; get; }
        public long TotalDistance { set; get; }
        public long Latitude { set; get; }
        public long Longitude { set; get; }
        public string Outlet_Code { set; get; }
        public string Outlet_Name { set; get; }
        public string Outlet_Latitude { set; get; }
        public string Outlet_Longitude { set; get; }
    }

    public class RouteValue1
    {
        public string number { set; get; }
        public string Outlet_Code { set; get; }
        public string Outlet_Name { set; get; }
        public string lat { set; get; }
        public string lng { set; get; }
        public string Distance_Between { set; get; }
        public double Distance { set; get; }
        public string Duration { set; get; }
    }


    class LatLong
    {
        public int lad { get; set; }
        public int longs { get; set; }
    }

    class DataModel
    {
        public long[,] Locations = { };
        public int VehicleNumber = 1;
        public int Depot = 0;
    };
}
