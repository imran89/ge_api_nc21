﻿using Google.OrTools.ConstraintSolver;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using static WebAPI.Controllers.RouteTSPController;

namespace WebAPI.Models
{
    public class ExecuteData
    {
        public void GetData(string deCode)
        {
            try
            {
                string ts = "";
                dynamic result = null;
                List<RouteOptimizeModel> listdata = new List<RouteOptimizeModel>();
                using (SqlConnection cnn = new SqlConnection("Server=cintaid.database.windows.net;Database=B2b;User Id=thomas.benny;Password=Mcfurry.2011"))
                {
                    cnn.Open();
                    SqlCommand cmd = new SqlCommand("SP_Get_Route_TSP", cnn)
                    {
                        CommandTimeout = 30,
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.Add("@paramJson", SqlDbType.NVarChar).Value = deCode;

                    using (XmlReader reader = cmd.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            string s = reader.Value.ToString();
                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore
                            };

                            listdata = JsonConvert.DeserializeObject<List<RouteOptimizeModel>>(s, settings);

                        }
                    }
                }


                //List<RouteOptimizeModel> listdata = Connections.ListData(data1);

                long[,] intarray_d = new long[listdata.Count, 2];

                for (int i = 0; i < listdata.Count; ++i)
                {
                    for (int j = 0; j < 2; ++j)
                    {
                        var int1 = listdata[i].Latitude;
                        var int2 = listdata[i].Longitude;
                        if (j == 0)
                        {
                            intarray_d[i, j] = int1;
                        }
                        else
                        {
                            intarray_d[i, j] = int2;
                        }
                    }
                }

                if (listdata.Count == 0)
                {
                    //return "0";
                }

                // Instantiate the data problem.
                // [START data]
                DataModel data = new DataModel();
                data.Locations = intarray_d;
                // [END data]

                // Create Routing Index Manager
                // [START index_manager]
                RoutingIndexManager manager = new RoutingIndexManager(
                    data.Locations.GetLength(0),
                    data.VehicleNumber,
                    data.Depot);
                // [END index_manager]

                // Create Routing Model.
                // [START routing_model]
                RoutingModel routing = new RoutingModel(manager);
                // [END routing_model]

                //// Define cost of each arc.
                //// [START transit_callback]
                //long[,] distanceMatrix = ComputeEuclideanDistanceMatrix(data.Locations);
                //int transitCallbackIndex = routing.RegisterTransitCallback(
                //  (long fromIndex, long toIndex) =>
                //  {
                //      // Convert from routing variable Index to distance matrix NodeIndex.
                //      var fromNode = manager.IndexToNode(fromIndex);
                //      var toNode = manager.IndexToNode(toIndex);
                //      return distanceMatrix[fromNode, toNode];
                //  }
                //);
                //// [END transit_callback]

                //// [START arc_cost]
                //routing.SetArcCostEvaluatorOfAllVehicles(transitCallbackIndex);
                //// [END arc_cost]

                // Setting first solution heuristic.
                // [START parameters]
                RoutingSearchParameters searchParameters =
                  operations_research_constraint_solver.DefaultRoutingSearchParameters();
                searchParameters.FirstSolutionStrategy =
                  FirstSolutionStrategy.Types.Value.PathCheapestArc;
                // [END parameters]

                // Solve the problem.
                // [START solve]
                Assignment solution = routing.SolveWithParameters(searchParameters);
                // [END solve]

                // Print solution on console.
                // [START print_solution]
                List<RouteValue> DataListRoute = PrintSolution(routing, manager, solution);
                // [END print_solution]

                for (int h = 0; h < DataListRoute.Count; ++h)
                {
                    for (int i = 0; i < listdata.Count; ++i)
                    {
                        if (DataListRoute[h].Index == i)
                        {
                            DataListRoute[h].Latitude = listdata[i].Latitude;
                            DataListRoute[h].Longitude = listdata[i].Longitude;
                            DataListRoute[h].Outlet_Code = listdata[i].Outlet_Code;
                            DataListRoute[h].Outlet_Name = listdata[i].Outlet_Name;
                            DataListRoute[h].Outlet_Latitude = listdata[i].Outlet_Latitude;
                            DataListRoute[h].Outlet_Longitude = listdata[i].Outlet_Longitude;
                        }
                    }
                }

                //json = JsonConvert.SerializeObject(DataListRoute);
                string json = JsonConvert.SerializeObject(DataListRoute);

                List<RouteValue1> bisa = new List<RouteValue1>();

                for (int h = 0; h < DataListRoute.Count; ++h)
                {
                    RouteValue1 bisa1 = new RouteValue1();
                    GeoCoordinate ke1 = new GeoCoordinate();
                    GeoCoordinate ke2 = new GeoCoordinate();

                    ke1.Latitude = Convert.ToDouble(DataListRoute[h].Outlet_Latitude);
                    ke1.Longitude = Convert.ToDouble(DataListRoute[h].Outlet_Longitude);

                    //GeoCoordinate ke1 = new GeoCoordinate(Convert.ToDouble(DataListRoute[h].Outlet_Latitude), Convert.ToDouble(DataListRoute[h].Outlet_Longitude));

                    //GeoCoordinate ke2 = new GeoCoordinate(Convert.ToDouble(DataListRoute[h + 1].Outlet_Latitude), Convert.ToDouble(DataListRoute[h + 1].Outlet_Longitude));

                    if (h + 1 < DataListRoute.Count)
                    {
                        ke2.Latitude = Convert.ToDouble(DataListRoute[h + 1].Outlet_Latitude);
                        ke2.Longitude = Convert.ToDouble(DataListRoute[h + 1].Outlet_Longitude);
                        bisa1.Distance_Between = "Jarak antara " + DataListRoute[h].Index + " ke " + DataListRoute[h + 1].Index;
                        //GeoCoordinate ke2 = new GeoCoordinate(Convert.ToDouble(DataListRoute[h + 1].Outlet_Latitude), Convert.ToDouble(DataListRoute[h + 1].Outlet_Longitude));
                    }
                    else
                    {
                        bisa1.Distance_Between = "Jarak antara " + DataListRoute[h].Index + " ke " + DataListRoute[0].Index;
                        ke2.Latitude = Convert.ToDouble(DataListRoute[0].Outlet_Latitude);
                        ke2.Longitude = Convert.ToDouble(DataListRoute[0].Outlet_Longitude);
                        //GeoCoordinate ke2 = new GeoCoordinate(Convert.ToDouble(DataListRoute[h + 1].Outlet_Latitude), Convert.ToDouble(DataListRoute[h + 1].Outlet_Longitude));
                    }

                    var geotool = new GeoCoordinateTool();
                    var distance = geotool.Distance(ke1, ke2, 1);


                    bisa1.Outlet_Code = DataListRoute[h].Outlet_Code;
                    bisa1.Outlet_Name = DataListRoute[h].Outlet_Name;
                    bisa1.number = Convert.ToString(h);
                    //bisa1.number = Convert.ToString(DataListRoute[h].Index);
                    bisa1.lat = DataListRoute[h].Outlet_Latitude;
                    bisa1.lng = DataListRoute[h].Outlet_Longitude;

                    bisa1.Distance = distance;

                    bisa.Add(bisa1);
                }

                json1 = JsonConvert.SerializeObject(bisa);

                //return json1;
            }
            catch (Exception e)
            {
                string msg = e.Message.ToString();
            }
        }

        
        // [END euclidean_distance]

        // [START solution_printer]
        /// <summary>
        ///   Print the solution.
        /// </summary>
        public static List<RouteValue> PrintSolution(
            in RoutingModel routing,
            in RoutingIndexManager manager,
            in Assignment solution)
        {
            System.Diagnostics.Debug.WriteLine("Route Terbaik untuk Annisa");
            System.Diagnostics.Debug.WriteLine("Objective: {0}", solution.ObjectiveValue());
            // Inspect solution.
            System.Diagnostics.Debug.WriteLine("Route:");
            long routeDistance = 0;
            var index = routing.Start(0);

            List<RouteValue> Hsl = new List<RouteValue>();

            while (routing.IsEnd(index) == false)
            {
                System.Diagnostics.Debug.WriteLine("{0} -> ", manager.IndexToNode((int)index));
                RouteValue itm = new RouteValue();
                itm.Index = Convert.ToInt32(index);

                var previousIndex = index;
                index = solution.Value(routing.NextVar(index));
                routeDistance += routing.GetArcCostForVehicle(previousIndex, index, 0);

                itm.Distance = Convert.ToInt32(routing.GetArcCostForVehicle(previousIndex, index, 0));
                itm.TotalDistance = Convert.ToInt32(routeDistance);
                Hsl.Add(itm);
            }

            System.Diagnostics.Debug.WriteLine("{0}", manager.IndexToNode((int)index));
            System.Diagnostics.Debug.WriteLine("Route distance: {0}m", routeDistance);

            return Hsl;
        }
        // [END solution_printer]

        public double Distance(GeoCoordinate loc1, GeoCoordinate loc2, int type)
        {
            //1- miles, other km
            //Use 3960 if you want miles; use 6371 if you want km
            double R = (type == 1) ? 3960 : 6371;          // R is earth radius.
            double dLat = this.toRadian(loc2.Latitude - loc1.Latitude);
            double dLon = this.toRadian(loc2.Longitude - loc1.Longitude);

            double a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) + Math.Cos(this.toRadian(loc1.Latitude)) * Math.Cos(this.toRadian(loc2.Latitude)) * Math.Sin(dLon / 2) * Math.Sin(dLon / 2);

            double c = 2 * Math.Asin(Math.Min(1, Math.Sqrt(a)));
            double d = R * c;

            return d;
        }

        private double toRadian(double val)
        {
            return (Math.PI / 180) * val;
        }


    }
}
