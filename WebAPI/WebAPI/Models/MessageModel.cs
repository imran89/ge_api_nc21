﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class MessageModel
    {
        public string Status { get; set; }
        public int ID { get; set; }
        public int Paid { get; set; }
    }
}
