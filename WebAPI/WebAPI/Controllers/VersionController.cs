﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VersionController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public string Get()
        {
            //return @"{""Version_Code"":""1.0.2"",""Version_Name"":""OrTools NC 2.1 Improvement""}";
            //return @"{""Version_Code"":""1.0.4"",""Version_Name"":""OrTools NC 2.2 InfoDevice""}";
            //return @"{""Version_Code"":""1.0.5"",""Version_Name"":""OrTools NC 2.1 Visit""}";
            //return @"{""Version_Code"":""1.0.6"",""Version_Name"":""OrTools NC 2.1 Improve Delete""}";
            //return @"{""Version_Code"":""1.0.7"",""Version_Name"":""OrTools NC 2.1 Batch number""}";
            //return @"{""Version_Code"":""1.0.8"",""Version_Name"":""OrTools NC 2.1 Outlet Updt Area""}";
            //return @"{""Version_Code"":""1.0.9"",""Version_Name"":""OrTools NC 2.1 Area By Company 2""}";
            //return @"{""Version_Code"":""1.0.10"",""Version_Name"":""OrTools NC 2.1 Monitor Summary""}";x
            return @"{""Version_Code"":""1.0.11"",""Version_Name"":""OrTools NC 2.1 Login with OTP""}";
        }

    }
}
