﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Belgrade.SqlClient;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        

        // GET api/values
        [HttpGet]
        public string Get()
        {
            return "Hello world";
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        //[AllowAnonymous]
        [HttpPost("StockItem")]
        public ActionResult Post()
        {

            var response = "dapat";// new SingleResponse<ExecuteData>();
            return Ok();

        }



        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
    public class AreaModel
    {
        public int AreaID { get; set; }
        public string AreaCode { get; set; }
        public string AreaName { get; set; }
    }
}
