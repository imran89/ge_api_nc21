﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebAPI.Models.InfoDevice;
using WebAPI.Services;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InfoDeviceController : ControllerBase
    {

        [HttpGet]
        public string Get()
        {
            //return @"{""Version_Code"":""1.0.2"",""Version_Name"":""OrTools NC 2.1 Improvement""}";
            return @"{""Version_Code"":""1.0.3"",""Version_Name"":""OrTools NC 2.2 InfoDevice""}";
        }

        //private readonly IInfoDeviceServices _services;
        [HttpPost("{Username}/{Password}/{dt}/{keys}")]
        public ActionResult<string> Add(string Username, string Password, string dt, string keys)
        {
            try
            {
                string paramJson = new StreamReader(Request.Body).ReadToEnd();
                if (keys == WebAPI.CLS_FUNCT.Base64Encode(Username + WebAPI.CLS_FUNCT.cintaidkeys + dt))
                {
                    using (SqlConnection cnn = new SqlConnection("Server=cintaid.database.windows.net;Database=B2b;User Id=thomas.benny;Password=Mcfurry.2011"))
                    {
                        cnn.Open();
                        SqlCommand cmd = new SqlCommand("Ins_M_DeviceInfo_Json", cnn)
                        {
                            CommandTimeout = 30,
                            CommandType = CommandType.StoredProcedure
                        };

                        cmd.Parameters.Add("@M_DeviceInfo_Json", SqlDbType.NVarChar).Value = paramJson;

                        using (XmlReader reader = cmd.ExecuteXmlReader())
                        {
                            while (reader.Read())
                            {
                                var s = reader.Value.ToString();
                                return s.ToString();
                            }
                        }
                    }
                }
                return @"{}";
            }
            catch(Exception ex)
            {
                return "Error : " + ex.ToString();
            }
        }      

    }
}