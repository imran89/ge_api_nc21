﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Xml;
using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebAPI.Models;
using WebAPI.Models.Common;
using WebAPI.Models.InfoDevice;
using WebAPI.Services;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SendEmailController : ControllerBase
    {
        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{paramJson}")]
        public async Task<string> Get(string Username, string Password, string dt, string keys, string paramJson)
        {
            try
            {
                if (keys == WebAPI.CLS_FUNCT.Base64Encode(Username + WebAPI.CLS_FUNCT.cintaidkeys + dt))
                {
                    string deCode = WebAPI.CLS_FUNCT.Base64Decode(paramJson);

                    string jsonPaid = "";
                    jsonPaid = CommonModel.ReadCintaID("SP_GET_TR_SALES", deCode, CLS_FUNCT.conn2);


                    List<MessageModel> sm = new List<MessageModel>();
                    sm = JsonConvert.DeserializeObject<List<MessageModel>>(jsonPaid);

                    if (sm[0].Paid == 1)
                    {
                        List<SendEmailModel> listSEM = new List<SendEmailModel>();
                        string jsonSE = "";
                        jsonSE = CommonModel.ReadCintaID("SP_Get_SendEmail", deCode, CLS_FUNCT.conn2);

                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };

                        listSEM = JsonConvert.DeserializeObject<List<SendEmailModel>>(jsonSE, settings);

                        string vBody = "";
                        string vItems = "";
                        string vCategory = "";
                        vCategory = listSEM[0].PYM_CATEGORY.ToString();
                        
                        //Title
                        if((vCategory == "RESTOGO") || (vCategory == "SERVGO") || (vCategory == "STOREGO"))
                        {
                            vBody += listSEM[0].Merchant_Name.ToString();
                            //Item
                            for (int i = 0; i < listSEM[0].Detail.Count(); i++)
                            {
                                vItems += "\nItem Name : " + listSEM[0].Detail[i].Sales_Item_Name.ToString() +
                                            "\tQty : " + listSEM[0].Detail[i].Sales_Qty +
                                            "\tPrice : " + listSEM[0].Detail[i].Sales_Price;
                            }
                        }
                        else
                        {
                            vBody += listSEM[0].PYM_CATEGORY.ToString();
                        }

                        vBody += "\nTransID : " + listSEM[0].PYM_ID;
                        vBody += "\nTanggal Transaksi : " + Convert.ToString(listSEM[0].PYM_LAST_ON.ToString("dd-MM-yyyy HH:mm"));

                        if (vItems.ToString() != string.Empty)
                        {
                            vBody += vItems.ToString();
                        }
                        vBody += "Admin : " + listSEM[0].PYM_UNIQUE_NO.ToString();
                        vBody += "\nTotal Amount : " + listSEM[0].PYM_UNIQUE_AMOUNT;

                        ////===========PROSES SEND EMAIL===============
                        string fromaddr = "cs@cinta.id"; //"goldeneye.cintaid@gmail.com";//"cs @cinta.id";
                        string password = "Good.P@$$w0rd";// "cinta2018";// "Good.P@$$w0rd";
                        string toaddr = "yudith.purnama@cinta.id";//TO ADDRESS HERE

                        MailMessage msg = new MailMessage();
                        msg.From = new MailAddress(fromaddr);
                        msg.To.Add(new MailAddress("yudith.purnama@cinta.id"));
                        msg.CC.Add("cs@cinta.id");
                        msg.Subject = "Report Transaction CintaID";
                        msg.Body = vBody.ToString();
                        SmtpClient smtp = new SmtpClient();
                        smtp.Host = "mail.cinta.id";// "smtp.gmail.com";
                        smtp.Port = 587;
                        smtp.UseDefaultCredentials = false;
                        smtp.EnableSsl = true;
                        NetworkCredential nc = new NetworkCredential(fromaddr, password);
                        smtp.Credentials = nc;
                        smtp.Send(msg);
                        return @"{""Status"":""Sukses Terkirim""}";
                    }

                }
                return @"{}";
            }catch(Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", keys.ToString() + ", Error : " + ex.ToString());
                return "Error : " + ex.ToString();
            }
        }

        


    }

}