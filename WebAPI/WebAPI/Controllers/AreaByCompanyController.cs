﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebAPI.Models.InfoDevice;
using WebAPI.Services;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AreaByCompanyController : ControllerBase
    {

        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Tipe}/{day}/{Long}/{Lat}/{Search}/{Company_ID}")]
        public async Task<string> Get(string Username, string Password, string dt, string keys, bool Tipe, int day, float Long, float Lat, string Search, Int32 Company_ID)
        {
            try
            {
                if (keys == WebAPI.CLS_FUNCT.Base64Encode(Username + WebAPI.CLS_FUNCT.cintaidkeys + dt))
                {
                    using (SqlConnection cnn = new SqlConnection(CLS_FUNCT.conn))
                    {
                        cnn.Open();
                        SqlCommand cmd = new SqlCommand("SP_Get_Area_ByCompany", cnn)
                        {
                            CommandTimeout = 30,
                            CommandType = CommandType.StoredProcedure
                        };

                        cmd.Parameters.Add("@username", SqlDbType.NVarChar).Value = Username;
                        cmd.Parameters.Add("@tipe", SqlDbType.NVarChar).Value = Tipe;
                        cmd.Parameters.Add("@day", SqlDbType.NVarChar).Value = day;
                        cmd.Parameters.Add("@long", SqlDbType.Float).Value = Long;
                        cmd.Parameters.Add("@lat", SqlDbType.Float).Value = Lat;
                        cmd.Parameters.Add("@Search", SqlDbType.NVarChar).Value = Search;
                        cmd.Parameters.Add("@Company_ID", SqlDbType.NVarChar).Value = Company_ID;

                        using (XmlReader reader = cmd.ExecuteXmlReader())
                        {
                            while (reader.Read())
                            {
                                string s = reader.Value.ToString();
                                var settings = new JsonSerializerSettings
                                {
                                    NullValueHandling = NullValueHandling.Ignore,
                                    MissingMemberHandling = MissingMemberHandling.Ignore
                                };

                                return s.ToString();

                            }
                        }
                        return @"{}";
                    }
                    return @"{}"; ;
                }
                return @"{}";
            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", keys.ToString() + ", Error : " + ex.ToString());
                return @"{""Message"":""Error  "" " + ex.ToString() + "}";  //"Error : " + ex.ToString();
            }

        }



    }
}