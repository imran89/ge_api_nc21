﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebAPI.Models.InfoDevice;
using WebAPI.Services;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MonitorController : ControllerBase
    {

        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Nik}/{Company_ID}/{TransDate}")]
        public async Task<string> Get(string Username, string Password, string dt, string keys, string Nik, Int32 Company_ID, DateTime Transdate)
        {
            try
            {
                if (keys == WebAPI.CLS_FUNCT.Base64Encode(Username + WebAPI.CLS_FUNCT.cintaidkeys + dt))
                {
                    using (SqlConnection cnn = new SqlConnection(CLS_FUNCT.conn))
                    {
                        cnn.Open();
                        SqlCommand cmd = new SqlCommand("SP_Monitor_TL", cnn)
                        {
                            CommandTimeout = 30,
                            CommandType = CommandType.StoredProcedure
                        };

                        cmd.Parameters.Add("@Nik", SqlDbType.NVarChar).Value = Nik;
                        cmd.Parameters.Add("@Transdate", SqlDbType.NVarChar).Value = Transdate;
                        cmd.Parameters.Add("@Company_ID", SqlDbType.NVarChar).Value = Company_ID;

                        using (XmlReader reader = cmd.ExecuteXmlReader())
                        {
                            while (reader.Read())
                            {
                                string s = reader.Value.ToString();
                                var settings = new JsonSerializerSettings
                                {
                                    NullValueHandling = NullValueHandling.Ignore,
                                    MissingMemberHandling = MissingMemberHandling.Ignore
                                };

                                return s.ToString();

                            }
                        }
                    }
                    return @"{}";;
                }
                return @"{}";
            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", keys.ToString() + ", Error : " + ex.ToString());
                return "Error : " + ex.ToString();
            }
        }

        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Search}/{Nik}/{Leader_Nik}/{Company_ID}/{TransDate}")]
        public async Task<string> Get(string Username, string Password, string dt, string keys, string Search, string Nik, string Leader_Nik, Int32 Company_ID, DateTime Transdate)
        {
            try
            {
                if (keys == WebAPI.CLS_FUNCT.Base64Encode(Username + WebAPI.CLS_FUNCT.cintaidkeys + dt))
                {
                    using (SqlConnection cnn = new SqlConnection(CLS_FUNCT.conn))
                    {
                        cnn.Open();
                        SqlCommand cmd = new SqlCommand("SP_Monitor_TL_Leader", cnn)
                        {
                            CommandTimeout = 30,
                            CommandType = CommandType.StoredProcedure
                        };

                        cmd.Parameters.Add("@Nik", SqlDbType.NVarChar).Value = Nik;
                        cmd.Parameters.Add("@Leader_Nik", SqlDbType.NVarChar).Value = Leader_Nik;
                        cmd.Parameters.Add("@Transdate", SqlDbType.NVarChar).Value = Transdate;
                        cmd.Parameters.Add("@Company_ID", SqlDbType.NVarChar).Value = Company_ID;

                        using (XmlReader reader = cmd.ExecuteXmlReader())
                        {
                            while (reader.Read())
                            {
                                string s = reader.Value.ToString();
                                var settings = new JsonSerializerSettings
                                {
                                    NullValueHandling = NullValueHandling.Ignore,
                                    MissingMemberHandling = MissingMemberHandling.Ignore
                                };

                                return s.ToString();

                            }
                        }
                    }
                    return @"{}";;
                }
                return @"{}";
            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", keys.ToString() + ", Error : " + ex.ToString());
                return @"{""Message"":""Error  "" " + ex.ToString() + "}";  //"Error : " + ex.ToString();
            }

        }


        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Nik}/{Company_ID}")]
        public async Task<string> Get(string Username, string Password, string dt, string keys, string Nik, Int32 Company_ID)
        {
            try
            {
                if (keys == WebAPI.CLS_FUNCT.Base64Encode(Username + WebAPI.CLS_FUNCT.cintaidkeys + dt))
                {
                    using (SqlConnection cnn = new SqlConnection(CLS_FUNCT.conn))
                    {
                        cnn.Open();
                        SqlCommand cmd = new SqlCommand("SP_Monitor_TL_ALLDate", cnn)
                        {
                            CommandTimeout = 30,
                            CommandType = CommandType.StoredProcedure
                        };

                        cmd.Parameters.Add("@Nik", SqlDbType.NVarChar).Value = Nik;
                        cmd.Parameters.Add("@Company_ID", SqlDbType.NVarChar).Value = Company_ID;

                        using (XmlReader reader = cmd.ExecuteXmlReader())
                        {
                            while (reader.Read())
                            {
                                string s = reader.Value.ToString();
                                var settings = new JsonSerializerSettings
                                {
                                    NullValueHandling = NullValueHandling.Ignore,
                                    MissingMemberHandling = MissingMemberHandling.Ignore
                                };

                                return s.ToString();

                            }
                        }
                    }
                    return @"{}";;
                }
                return @"{}";
            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", keys.ToString() + ", Error : " + ex.ToString());
                return @"{""Message"":""Error  "" " + ex.ToString() + "}";  //"Error : " + ex.ToString();
            }


        }

        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Nik}/{District}/{Search}/{Company_ID}")]
        public async Task<string> Get(string Username, string Password, string dt, string keys, string Nik, string District, string Search, Int32 Company_ID)
        {
            try
            {
                if (keys == WebAPI.CLS_FUNCT.Base64Encode(Username + WebAPI.CLS_FUNCT.cintaidkeys + dt))
                {
                    using (SqlConnection cnn = new SqlConnection(CLS_FUNCT.conn))
                    {
                        cnn.Open();
                        SqlCommand cmd = new SqlCommand("SP_Get_Outlet", cnn)
                        {
                            CommandTimeout = 30,
                            CommandType = CommandType.StoredProcedure
                        };

                        cmd.Parameters.Add("@Nik", SqlDbType.NVarChar).Value = Nik;
                        cmd.Parameters.Add("@District", SqlDbType.NVarChar).Value = District;
                        cmd.Parameters.Add("@Search", SqlDbType.NVarChar).Value = Search;
                        cmd.Parameters.Add("@Company_ID", SqlDbType.NVarChar).Value = Company_ID;

                        using (XmlReader reader = cmd.ExecuteXmlReader())
                        {
                            while (reader.Read())
                            {
                                string s = reader.Value.ToString();
                                var settings = new JsonSerializerSettings
                                {
                                    NullValueHandling = NullValueHandling.Ignore,
                                    MissingMemberHandling = MissingMemberHandling.Ignore
                                };

                                return s.ToString();

                            }
                        }
                    }
                    return @"{}";;
                }
                return @"{}";
            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", keys.ToString() + ", Error : " + ex.ToString());
                return @"{""Message"":""Error  "" " + ex.ToString() + "}";  //"Error : " + ex.ToString();
            }


        }
    }
}