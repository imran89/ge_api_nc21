﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebAPI.Models.InfoDevice;
using WebAPI.Services;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BatchNumberController : ControllerBase
    {

        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{paramJson}")]
        public async Task<string> Get(string Username, string Password, string dt, string keys, string paramJson)
        {
            try
            {
                string deCode = "";
                if (keys == WebAPI.CLS_FUNCT.Base64Encode(Username + WebAPI.CLS_FUNCT.cintaidkeys + dt))
                {
                    deCode = CLS_FUNCT.Base64Decode(paramJson);
                    return CLS_FUNCT.GetSP("SP_Get_M_Batch_Number", "@paramJson", deCode);
                }
                return @"{}";
            }catch(Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", keys.ToString() + ", Error : " + ex.ToString());
                return "Error : " + ex.ToString();
            }
        }

        

    }
}