﻿
using Belgrade.SqlClient;
using Google.OrTools.ConstraintSolver;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Xml;
using WebAPI.Models;
//using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RouteTSPController : ControllerBase
    {
       

        [HttpGet("{Username}/{Password}/{dt}/{keys}/{paramJson}")]
        public async Task<string> Get(string Username, string Password, string dt, string keys, string paramJson)
        {
            try
            {
                if (keys == WebAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
                {
                    string deCode = "";
                    deCode = WebAPI.CLS_FUNCT.Base64Decode(paramJson);
                    List<RouteOptimizeModel> listdata = new List<RouteOptimizeModel>();
                    using (SqlConnection cnn = new SqlConnection("Server=cintaid.database.windows.net;Database=B2b;User Id=thomas.benny;Password=Mcfurry.2011"))
                    {
                        cnn.Open();
                        SqlCommand cmd = new SqlCommand("SP_Get_Route_TSP", cnn)
                        {
                            CommandTimeout = 30,
                            CommandType = CommandType.StoredProcedure
                        };

                        cmd.Parameters.Add("@paramJson", SqlDbType.NVarChar).Value = deCode;

                        using (XmlReader reader = cmd.ExecuteXmlReader())
                        {
                            while (reader.Read())
                            {
                                string s = reader.Value.ToString();
                                var settings = new JsonSerializerSettings
                                {
                                    NullValueHandling = NullValueHandling.Ignore,
                                    MissingMemberHandling = MissingMemberHandling.Ignore
                                };

                                listdata = JsonConvert.DeserializeObject<List<RouteOptimizeModel>>(s, settings);

                            }
                        }
                    }

                    if (listdata.Count != 0)
                    {
                        long[,] intarray_d = new long[listdata.Count, 2];

                        for (int i = 0; i < listdata.Count; ++i)
                        {
                            for (int j = 0; j < 2; ++j)
                            {
                                var int1 = listdata[i].Latitude;
                                var int2 = listdata[i].Longitude;
                                if (j == 0)
                                {
                                    intarray_d[i, j] = int1;
                                }
                                else
                                {
                                    intarray_d[i, j] = int2;
                                }
                            }
                        }

                        DataModel data = new DataModel();
                        data.Locations = intarray_d;
                        // [END data]

                        // Create Routing Index Manager
                        // [START index_manager]
                        RoutingIndexManager manager = new RoutingIndexManager(
                            data.Locations.GetLength(0),
                            data.VehicleNumber,
                            data.Depot);
                        // [END index_manager]

                        // Create Routing Model.
                        // [START routing_model]

                        long[,] locations = new long[,] { };
                        RoutingModel routing = new RoutingModel(manager);


                        int locationNumber = locations.GetLength(0);


                        // Define cost of each arc.
                        // [START transit_callback]
                        long[,] distanceMatrix = ComputeEuclideanDistanceMatrix(data.Locations);
                        int transitCallbackIndex = routing.RegisterTransitCallback(
                          (long fromIndex, long toIndex) =>
                          {
                              // Convert from routing variable Index to distance matrix NodeIndex.
                              var fromNode = manager.IndexToNode(fromIndex);
                              var toNode = manager.IndexToNode(toIndex);
                              return distanceMatrix[fromNode, toNode];
                          }
                        );
                        // [END transit_callback]

                        // [START arc_cost]
                        routing.SetArcCostEvaluatorOfAllVehicles(transitCallbackIndex);
                        // [END arc_cost]

                        // Setting first solution heuristic.
                        // [START parameters]
                        RoutingSearchParameters searchParameters =
                          operations_research_constraint_solver.DefaultRoutingSearchParameters();
                        searchParameters.FirstSolutionStrategy =
                          FirstSolutionStrategy.Types.Value.PathCheapestArc;
                        // [END parameters]

                        // Solve the problem.
                        // [START solve]
                        Assignment solution = routing.SolveWithParameters(searchParameters);
                        // [END solve]

                        // Print solution on console.
                        // [START print_solution]
                        List<RouteValue> DataListRoute = ExecuteData.PrintSolution(routing, manager, solution);
                        // [END print_solution]

                        for (int h = 0; h < DataListRoute.Count; ++h)
                        {
                            for (int i = 0; i < listdata.Count; ++i)
                            {
                                if (DataListRoute[h].Index == i)
                                {
                                    DataListRoute[h].Latitude = listdata[i].Latitude;
                                    DataListRoute[h].Longitude = listdata[i].Longitude;
                                    DataListRoute[h].Outlet_Code = listdata[i].Outlet_Code;
                                    DataListRoute[h].Outlet_Name = listdata[i].Outlet_Name;
                                    DataListRoute[h].Outlet_Latitude = listdata[i].Outlet_Latitude;
                                    DataListRoute[h].Outlet_Longitude = listdata[i].Outlet_Longitude;
                                }
                            }
                        }

                        //json = JsonConvert.SerializeObject(DataListRoute);
                        string json = JsonConvert.SerializeObject(DataListRoute);

                        List<RouteValue1> bisa = new List<RouteValue1>();

                        for (int h = 0; h < DataListRoute.Count; ++h)
                        {
                            RouteValue1 bisa1 = new RouteValue1();
                            GeoCoordinate ke1 = new GeoCoordinate();
                            GeoCoordinate ke2 = new GeoCoordinate();

                            string url = "";

                            if (h < DataListRoute.Count - 1)
                            {
                                url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" + DataListRoute[h].Outlet_Latitude + "," + DataListRoute[h].Outlet_Longitude + "&destinations=" + DataListRoute[h + 1].Outlet_Latitude + "," + DataListRoute[h + 1].Outlet_Longitude + "&departure_time=now&key=AIzaSyBKDtW47ZKzT5JPduQvi3gUFNHNZmXk-FU";
                            }
                            else
                            {
                                url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" + DataListRoute[h].Outlet_Latitude + "," + DataListRoute[h].Outlet_Longitude + "&destinations=" + DataListRoute[0].Outlet_Latitude + "," + DataListRoute[0].Outlet_Longitude + "&departure_time=now&key=AIzaSyBKDtW47ZKzT5JPduQvi3gUFNHNZmXk-FU";
                            }

                            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                            WebResponse response = request.GetResponse();
                            Stream dataStream = response.GetResponseStream();
                            StreamReader sreader = new StreamReader(dataStream);
                            var responsereader = sreader.ReadToEnd();
                            var x = JObject.Parse(responsereader);
                            var success = x["rows"][0]["elements"][0]["duration"]["text"];

                            bisa1.Duration = Convert.ToString(success);

                            ke1.Latitude = Convert.ToDouble(DataListRoute[h].Outlet_Latitude);
                            ke1.Longitude = Convert.ToDouble(DataListRoute[h].Outlet_Longitude);

                            //GeoCoordinate ke1 = new GeoCoordinate(Convert.ToDouble(DataListRoute[h].Outlet_Latitude), Convert.ToDouble(DataListRoute[h].Outlet_Longitude));

                            //GeoCoordinate ke2 = new GeoCoordinate(Convert.ToDouble(DataListRoute[h + 1].Outlet_Latitude), Convert.ToDouble(DataListRoute[h + 1].Outlet_Longitude));

                            if (h + 1 < DataListRoute.Count)
                            {
                                ke2.Latitude = Convert.ToDouble(DataListRoute[h + 1].Outlet_Latitude);
                                ke2.Longitude = Convert.ToDouble(DataListRoute[h + 1].Outlet_Longitude);
                                bisa1.Distance_Between = DataListRoute[h].Outlet_Name + "|" + DataListRoute[h + 1].Outlet_Name;
                                //GeoCoordinate ke2 = new GeoCoordinate(Convert.ToDouble(DataListRoute[h + 1].Outlet_Latitude), Convert.ToDouble(DataListRoute[h + 1].Outlet_Longitude));
                            }
                            else
                            {
                                bisa1.Distance_Between = DataListRoute[h].Outlet_Name + "|" + DataListRoute[0].Outlet_Name;
                                ke2.Latitude = Convert.ToDouble(DataListRoute[0].Outlet_Latitude);
                                ke2.Longitude = Convert.ToDouble(DataListRoute[0].Outlet_Longitude);
                                //GeoCoordinate ke2 = new GeoCoordinate(Convert.ToDouble(DataListRoute[h + 1].Outlet_Latitude), Convert.ToDouble(DataListRoute[h + 1].Outlet_Longitude));
                            }

                            var geotool = new GeoCoordinateTool();
                            var distance = geotool.Distance(ke1, ke2, 1);


                            bisa1.Outlet_Code = DataListRoute[h].Outlet_Code;
                            bisa1.Outlet_Name = DataListRoute[h].Outlet_Name;
                            bisa1.number = Convert.ToString(h);
                            //bisa1.number = Convert.ToString(DataListRoute[h].Index);
                            bisa1.lat = DataListRoute[h].Outlet_Latitude;
                            bisa1.lng = DataListRoute[h].Outlet_Longitude;

                            string DistanceKm = String.Format("{0:0.00}", distance * 1.609344);
                            bisa1.Distance = Convert.ToDouble(DistanceKm);

                            bisa.Add(bisa1);
                        }

                        return json1 = JsonConvert.SerializeObject(bisa);
                    }
                    else
                    {
                        return @"{}";
                    }
                }
                else
                {
                    return "Parameter does not match";
                }
            }
            catch(Exception ex)
            {
                return json1 = ex.ToString();
            }
            

        }


        string json = "";
        public static string json1 = "";


        static long[,] ComputeEuclideanDistanceMatrix(in long[,] locations)
        {
            // Calculate the distance matrix using Euclidean distance.
            int locationNumber = locations.GetLength(0);
            long[,] distanceMatrix = new long[locationNumber, locationNumber];
            for (int fromNode = 0; fromNode < locationNumber; fromNode++)
            {
                for (int toNode = 0; toNode < locationNumber; toNode++)
                {
                    if (fromNode == toNode)
                        distanceMatrix[fromNode, toNode] = 0;
                    else
                        distanceMatrix[fromNode, toNode] = (long)
                          Math.Sqrt(
                            Math.Pow(locations[toNode, 0] - locations[fromNode, 0], 2) +
                            Math.Pow(locations[toNode, 1] - locations[fromNode, 1], 2));
                }
            }
            return distanceMatrix;
        }



        // [START euclidean_distance]
        /// <summary>
        ///   Euclidean distance implemented as a callback. It uses an array of
        ///   positions and computes the Euclidean distance between the two
        ///   positions of two different indices.
        /// </summary>








    }

}
