﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebAPI.Models.InfoDevice;
using WebAPI.Services;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CollectionController : ControllerBase
    {


        //private readonly IInfoDeviceServices _services;
        [HttpPost("{Username}/{Password}/{dt}/{keys}")]
        public ActionResult<string> Add(string Username, string Password, string dt, string keys)
        {
            try
            {
                string paramJson = new StreamReader(Request.Body).ReadToEnd();
                if (keys == WebAPI.CLS_FUNCT.Base64Encode(Username + WebAPI.CLS_FUNCT.cintaidkeys + dt))
                {
                    string replayJson = CLS_FUNCT.PostSP("Updt_Tr_Collection_Json", "@UpdtObjectJson", paramJson);
                    return replayJson.ToString();
                }
                return @"{}";
            }
            catch(Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", keys.ToString() + ", Error : " + ex.ToString());
                return "Error : " + ex.ToString();
            }
        }

        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{paramJson}")]
        public async Task<string> Get(string Username, string Password, string dt, string keys, string paramJson)
        {
            try
            {
                string deCode = "";
                if (keys == WebAPI.CLS_FUNCT.Base64Encode(Username + WebAPI.CLS_FUNCT.cintaidkeys + dt))
                {
                    deCode = CLS_FUNCT.Base64Decode(paramJson);
                    return CLS_FUNCT.GetSP("SP_Get_Tr_Collection", "@paramJson", deCode);
                }
                return @"{}";
            }catch(Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", keys.ToString() + ", Error : " + ex.ToString());
                return "Error : " + ex.ToString();
            }
        }

        // DELETE api/Todo/5
        [HttpPost("{Username}/{Password}/{dt}/{keys}/{delete}")]
        public ActionResult<string> DeleteUpdate(string Username, string Password, string dt, string keys, string delete)
        {

            try
            {
                string paramJson = new StreamReader(Request.Body).ReadToEnd();
                if (keys == WebAPI.CLS_FUNCT.Base64Encode(Username + WebAPI.CLS_FUNCT.cintaidkeys + dt))
                {
                    string replayJson = CLS_FUNCT.PostSP("Del_Tr_Collection", "@paramJson", paramJson);
                    return replayJson.ToString();
                }
                return @"{}";
            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", keys.ToString() + ", Error : " + ex.ToString());
                return "Error : " + ex.ToString();
            }


        }

    }
}