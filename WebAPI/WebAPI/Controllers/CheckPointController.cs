﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebAPI.Models.InfoDevice;
using WebAPI.Services;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CheckPointController : ControllerBase
    {

        [HttpGet]
        public string Get()
        {
            //return @"{""Version_Code"":""1.0.2"",""Version_Name"":""OrTools NC 2.1 Improvement""}";
            return @"{""Version_Code"":""1.0.3"",""Version_Name"":""OrTools NC 2.2 InfoDevice""}";
        }

        //private readonly IInfoDeviceServices _services;
        [HttpPost("{Username}/{Password}/{dt}/{keys}")]
        public ActionResult<string> Add(string Username, string Password, string dt, string keys)
        {
            try
            {
                string paramJson = new StreamReader(Request.Body).ReadToEnd();
                if (keys == WebAPI.CLS_FUNCT.Base64Encode(Username + WebAPI.CLS_FUNCT.cintaidkeys + dt))
                {
                    string replayJson = CLS_FUNCT.PostSP("Ins_Tr_Visit_Json", "@Tr_VisitJson", paramJson);
                    return replayJson.ToString();
                }
                return @"{}";
            }
            catch(Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", keys.ToString() + ", Error : " + ex.ToString());
                return "Error : " + ex.ToString();
            }
        }

        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Search}")]
        public async Task<string> Get(string Username, string Password, string dt, string keys, string Search)
        {
            try
            {
                if (keys == WebAPI.CLS_FUNCT.Base64Encode(Username + WebAPI.CLS_FUNCT.cintaidkeys + dt))
                {
                    using (SqlConnection cnn = new SqlConnection(CLS_FUNCT.conn))
                    {
                        cnn.Open();
                        SqlCommand cmd = new SqlCommand("SP_GetVisit", cnn)
                        {
                            CommandTimeout = 30,
                            CommandType = CommandType.StoredProcedure
                        };

                        cmd.Parameters.Add("@username", SqlDbType.NVarChar).Value = Username;
                        cmd.Parameters.Add("@Search", SqlDbType.NVarChar).Value = Search;


                        using (XmlReader reader = cmd.ExecuteXmlReader())
                        {

                            while (reader.Read())
                            {
                                string s = reader.Value.ToString();
                                var settings = new JsonSerializerSettings
                                {
                                    NullValueHandling = NullValueHandling.Ignore,
                                    MissingMemberHandling = MissingMemberHandling.Ignore
                                };

                                return s.ToString();

                            }

                            return @"{}";
                        }
                        //return @"{}";;
                    }
                    return @"{}";;
                }
                return @"{}";
            }
            catch(Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", keys.ToString() + ", Error : " + ex.ToString());
                return "Error : " + ex.ToString();
            }
        }

        private static void ReadSingleRow(IDataRecord record)
        {
            Console.WriteLine(String.Format("{0}, {1}", record[0], record[1]));
        }

        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Search}/{id}")]
        public async Task<string> Get(string Username, string Password, string dt, string keys, string Search, int id)
        {
            try
            {
                if (keys == WebAPI.CLS_FUNCT.Base64Encode(Username + WebAPI.CLS_FUNCT.cintaidkeys + dt))
                {
                    using (SqlConnection cnn = new SqlConnection(CLS_FUNCT.conn))
                    {
                        cnn.Open();
                        SqlCommand cmd = new SqlCommand("SP_GetVisit_ID", cnn)
                        {
                            CommandTimeout = 30,
                            CommandType = CommandType.StoredProcedure
                        };

                        cmd.Parameters.Add("@id", SqlDbType.NVarChar).Value = id;

                        using (XmlReader reader = cmd.ExecuteXmlReader())
                        {
                            while (reader.Read())
                            {
                                string s = reader.Value.ToString();
                                var settings = new JsonSerializerSettings
                                {
                                    NullValueHandling = NullValueHandling.Ignore,
                                    MissingMemberHandling = MissingMemberHandling.Ignore
                                };

                                return s.ToString();

                            }
                        }
                    }
                    return @"{}";;
                }
                return @"{}";
            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", keys.ToString() + ", Error : " + ex.ToString());
                return @"{""Message"":""Error  "" " + ex.ToString() + "}";  //"Error : " + ex.ToString();
            }

        }

        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Search}/{Id}/{day}/{Image}")]
        public async Task<string> Get(string Username, string Password, string dt, string keys, string Search, int Id, string Image)
        {
            try
            {
                if (keys == WebAPI.CLS_FUNCT.Base64Encode(Username + WebAPI.CLS_FUNCT.cintaidkeys + dt))
                {
                    using (SqlConnection cnn = new SqlConnection(CLS_FUNCT.conn))
                    {
                        cnn.Open();
                        SqlCommand cmd = new SqlCommand("SP_GetVisit_Image", cnn)
                        {
                            CommandTimeout = 30,
                            CommandType = CommandType.StoredProcedure
                        };

                        cmd.Parameters.Add("@Image", SqlDbType.NVarChar).Value = Image;

                        using (XmlReader reader = cmd.ExecuteXmlReader())
                        {
                            while (reader.Read())
                            {
                                string s = reader.Value.ToString();
                                var settings = new JsonSerializerSettings
                                {
                                    NullValueHandling = NullValueHandling.Ignore,
                                    MissingMemberHandling = MissingMemberHandling.Ignore
                                };

                                return s.ToString();

                            }
                        }
                    }
                    return @"{}";;
                }
                return @"{}";
            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", keys.ToString() + ", Error : " + ex.ToString());
                return @"{""Message"":""Error  "" " + ex.ToString() + "}";  //"Error : " + ex.ToString();
            }

        }

        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Tipe}/{day}/{Long}/{Lat}/{Search}/{Company_ID}")]
        public async Task<string> Get(string Username, string Password, string dt, string keys, bool Tipe, int day, float Long, float Lat, string Search, Int32 Company_ID)
        {
            try
            {
                if (keys == WebAPI.CLS_FUNCT.Base64Encode(Username + WebAPI.CLS_FUNCT.cintaidkeys + dt))
                {
                    using (SqlConnection cnn = new SqlConnection(CLS_FUNCT.conn))
                    {
                        cnn.Open();
                        SqlCommand cmd = new SqlCommand("SP_Daily_Route_Type", cnn)
                        {
                            CommandTimeout = 30,
                            CommandType = CommandType.StoredProcedure
                        };

                        cmd.Parameters.Add("@username", SqlDbType.NVarChar).Value = Username;
                        cmd.Parameters.Add("@tipe", SqlDbType.NVarChar).Value = Tipe;
                        cmd.Parameters.Add("@day", SqlDbType.NVarChar).Value = day;
                        cmd.Parameters.Add("@long", SqlDbType.Float).Value = Long;
                        cmd.Parameters.Add("@lat", SqlDbType.Float).Value = Lat;
                        cmd.Parameters.Add("@Search", SqlDbType.NVarChar).Value = Search;
                        cmd.Parameters.Add("@Company_ID", SqlDbType.NVarChar).Value = Company_ID;

                        using (XmlReader reader = cmd.ExecuteXmlReader())
                        {
                            while (reader.Read())
                            {
                                string s = reader.Value.ToString();
                                var settings = new JsonSerializerSettings
                                {
                                    NullValueHandling = NullValueHandling.Ignore,
                                    MissingMemberHandling = MissingMemberHandling.Ignore
                                };

                                return s.ToString();

                            }
                        }
                        return @"{}";
                    }
                    return @"{}";;
                }
                return @"{}";
            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", keys.ToString() + ", Error : " + ex.ToString());
                return @"{""Message"":""Error  "" " + ex.ToString() + "}";  //"Error : " + ex.ToString();
            }

        }

        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{day}/{Long}/{Lat}/{Company_ID}")]
        public async Task<string> Get(string Username, string Password, string dt, string keys, int day, float Long, float Lat, Int32 Company_ID)
        {
            try
            {
                if (keys == WebAPI.CLS_FUNCT.Base64Encode(Username + WebAPI.CLS_FUNCT.cintaidkeys + dt))
                {
                    using (SqlConnection cnn = new SqlConnection(CLS_FUNCT.conn))
                    {
                        cnn.Open();
                        SqlCommand cmd = new SqlCommand("SP_Daily_Route", cnn)
                        {
                            CommandTimeout = 30,
                            CommandType = CommandType.StoredProcedure
                        };

                        cmd.Parameters.Add("@username", SqlDbType.NVarChar).Value = Username;
                        cmd.Parameters.Add("@day", SqlDbType.NVarChar).Value = day;
                        cmd.Parameters.Add("@long", SqlDbType.Float).Value = Long;
                        cmd.Parameters.Add("@lat", SqlDbType.Float).Value = Lat;
                        cmd.Parameters.Add("@Company_ID", SqlDbType.NVarChar).Value = Company_ID;

                        using (XmlReader reader = cmd.ExecuteXmlReader())
                        {
                            while (reader.Read())
                            {
                                string s = reader.Value.ToString();
                                var settings = new JsonSerializerSettings
                                {
                                    NullValueHandling = NullValueHandling.Ignore,
                                    MissingMemberHandling = MissingMemberHandling.Ignore
                                };

                                return s.ToString();

                            }
                        }
                    }
                    return @"{}";;
                }
                return @"{}";
            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", keys.ToString() + ", Error : " + ex.ToString());
                return @"{""Message"":""Error  "" " + ex.ToString() + "}";  //"Error : " + ex.ToString();
            }

        }


        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{day}/{Long}/{Lat}/{Search}/{Company_ID}")]
        public async Task<string> Get(string Username, string Password, string dt, string keys, int day, float Long, float Lat, string Search, Int32 Company_ID)
        {
            try
            {
                if (keys == WebAPI.CLS_FUNCT.Base64Encode(Username + WebAPI.CLS_FUNCT.cintaidkeys + dt))
                {
                    using (SqlConnection cnn = new SqlConnection(CLS_FUNCT.conn))
                    {
                        cnn.Open();
                        SqlCommand cmd = new SqlCommand("SP_Daily_Route_Search", cnn)
                        {
                            CommandTimeout = 30,
                            CommandType = CommandType.StoredProcedure
                        };

                        cmd.Parameters.Add("@username", SqlDbType.NVarChar).Value = Username;
                        cmd.Parameters.Add("@day", SqlDbType.NVarChar).Value = day;
                        cmd.Parameters.Add("@long", SqlDbType.Float).Value = Long;
                        cmd.Parameters.Add("@lat", SqlDbType.Float).Value = Lat;
                        cmd.Parameters.Add("@Search", SqlDbType.NVarChar).Value = Search;
                        cmd.Parameters.Add("@Company_ID", SqlDbType.NVarChar).Value = Company_ID;

                        using (XmlReader reader = cmd.ExecuteXmlReader())
                        {
                            while (reader.Read())
                            {
                                string s = reader.Value.ToString();
                                var settings = new JsonSerializerSettings
                                {
                                    NullValueHandling = NullValueHandling.Ignore,
                                    MissingMemberHandling = MissingMemberHandling.Ignore
                                };

                                return s.ToString();

                            }
                        }
                    }
                    return @"{}";;
                }
                return @"{}";
            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", keys.ToString() + ", Error : " + ex.ToString());
                return @"{""Message"":""Error  "" " + ex.ToString() + "}";  //"Error : " + ex.ToString();
            }

        }

    }
}